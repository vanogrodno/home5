import React from "react";
import { Form,  Row, Col, Button, Input } from "antd";



const ExitTodoForm = ({ form, onFormSubmit }) => {
  const { getFieldDecorator } = form;

  // form submit handler
  const handleSubmit = e => {
    e.preventDefault();
    form.validateFields((err, todo) => {
      if (!err) {
        // сброс формы
        form.resetFields();

        // разварачиваем форму если все хорошо
        onFormSubmit(todo.name);
      }
    });
  };

  return (
    <Form
      onSubmit={e => handleSubmit(e)}

    >
      <Row >
          <Col>
              <Button type="primary" htmlType="submit" block>
                 Выйти
              </Button>
          </Col>
        <Col>

        </Col>

      </Row>
    </Form>
  );
};

export default Form.create({ name: "ExitTodoForm" })(ExitTodoForm);
