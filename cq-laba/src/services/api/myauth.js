import http from './http'

const signUp = data => {
    return http({
        url: 'https://lab.dev.cogniteq.com/api/auth/signUp',
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        }
    })
}

const signIn = data => {
    return http({
        url: 'https://lab.dev.cogniteq.com/api/auth/signIn',
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        }
    })
}

export {
    signUp,
    signIn
}