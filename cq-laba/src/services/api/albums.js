import http from './http'

const getAlbums = data => {
  return http({
    url: '/albums',
    method: 'get',
      headers: {
          'Content-type': 'application/json; charset=UTF-8',
          'token' : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Iml2YW51c2hrYTIxQHR1dC5ieSIsInVzZXJfaWQiOiI1ZTAzOWEzOThkOTg0MTdiNjE2Y2QzODciLCJpYXQiOjE1NzczMDQzODcsImV4cCI6MTU3NzM0MDM4N30.vAh05_6ErPtDHXo6AjSjZsQo2vCL5s6uMUHg05L3_Ho'
      },
    data
  })
}

const createAlbum = data => {
  return http({
    url: '/albums',
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  })
}

export {
  getAlbums,
  createAlbum
}
