import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {PostData} from '../../services/api/postdata';
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from 'react-router-dom';

const customHistory = createBrowserHistory();


class Login extends Component {
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',

        };
        this.login = this.login.bind(this);
        this.onChange = this.onChange.bind(this);
    }


    login() {
        if(this.state.email && this.state.password){
            PostData('signin',this.state).then((result) => {
                let responseJson = result;

                if(responseJson.token){
                    sessionStorage.setItem('token',JSON.stringify(responseJson));

                    const customHistory = createBrowserHistory();
                    customHistory.push('/Home');
                }



                })
            }
    }


    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }
    render() {




        return (


            <div className="row" id="Body">

                    <h4>Login</h4>
                <p><label>email</label></p>
                <p> <input type="text" name="email" onChange={this.onChange}/></p>
                    <p>   <label>Password</label></p>
                        <p>  <input type="password" name="password" onChange={this.onChange}/></p>
                            <p>    <input type="submit" value="Login" onClick={this.login}/></p>
                                <p>   <a href="/signup">Registration</a></p>

            </div>
        );
    }
}
export default Login;