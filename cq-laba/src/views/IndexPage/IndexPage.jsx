import React, { Fragment } from 'react'
import { Button } from '../../components'
import { Welcome } from './styled'
import blackBoxImage from './media/blackbox.png'
import history from '../../history'

const btnStyle = {
  maxWidth: 300,
  with: 300
}

const IndexPage = () => {
  return (
    <Fragment>
      <Welcome>
        <Welcome.Card>
          <img src={blackBoxImage} alt='' />
          <Welcome.CardTitle>Welcome to index page</Welcome.CardTitle>


            <br />
            <Button
                type='primary'
                style={btnStyle}
                onClick={() => history.push('/Login')}
            >
                Login
            </Button>
            <br />
            <Button
                type='primary'
                style={btnStyle}
                onClick={() => history.push('/Home')}
            >
                Todo
            </Button>
            <br />
        </Welcome.Card>
      </Welcome>
    </Fragment>
  )
}

export default IndexPage
