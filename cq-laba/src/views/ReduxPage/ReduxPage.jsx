import React, { Fragment } from 'react'
import Button from "../../components/Button";

class ReduxPage extends React.Component {
  state = {
    ip: '',
    label: '',
  };

  handleInputLabel = (e) => this.setState({label: e.target.value})
  handleInputIp = (e) => this.setState({ip: e.target.value})

  handleAddIp = () => {
    this.props.addIp({
      ip: this.state.ip,
      label: this.state.label
    })
  }

  render() {
    return (
      <Fragment>
        <h1>Redux</h1>

        <Fragment>
          <h2>Add new IP address</h2>
          <Fragment>
            <div>
              <p><b>IP address</b></p>
              <input type='text' value={this.state.ip} onChange={this.handleInputIp}/>
            </div>
            <div>
              <p><b>Label</b></p>
              <input type='text' value={this.state.label} onChange={this.handleInputLabel}/>
            </div>
            <p>
              <Button
                type='primary'
                onClick={this.handleAddIp}
              >
                Add
              </Button>
            </p>
          </Fragment>
        </Fragment>


        <Fragment>
        {this.props.ips.list.map((el) => (<div key={el.ip}>{el.ip} - {el.label}</div>))}
        </Fragment>

      </Fragment>
    )
  }
}

export default ReduxPage
