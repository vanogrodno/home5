import {
  ALBUMS_FAIL,
  ALBUMS_FETCHING,
  ALBUMS_SUCCESS,

  CREATE_ALBUM_FAIL,
  CREATE_ALBUM_SUCCESS
} from '../types'

import { api } from '../../services'

const getAlbums = () => dispatch => {
  dispatch({
    type: ALBUMS_FETCHING,
    payload: true
  })
  return api.albums.getAlbums()
    .then(({ data }) => {
      if (data) {
        dispatch({
          type: ALBUMS_SUCCESS,
          payload: data
        })
      }
      dispatch({
        type: ALBUMS_FETCHING,
        payload: false
      })
    }, (data) => {
      dispatch({
        type: ALBUMS_FAIL,
        payload: data
      })
    })
}

const createAlbum = (data) => dispatch => {
  dispatch({
    type: ALBUMS_FETCHING,
    payload: true
  })
  return api.albums.createAlbum(data)
    .then(({ data }) => {
      if (data) {
        dispatch({
          type: CREATE_ALBUM_SUCCESS,
          payload: data
        })
      }
      dispatch({
        type: ALBUMS_FETCHING,
        payload: false
      })
    }, (data) => {
      dispatch({
        type: CREATE_ALBUM_FAIL,
        payload: data
      })
    })
}


export {
  getAlbums,
  createAlbum
}
